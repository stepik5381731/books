from django.contrib.auth.models import User
from django.test import TestCase

from apps.store.logic import set_rating
from apps.store.models import UserBookRelation, Book


class SetRatingTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create(username='test username 1', first_name='Ivan', last_name='Petrov')
        user2 = User.objects.create(username='test username 2', first_name='Ivan', last_name='Sidor')
        user3 = User.objects.create(username='test username 3', first_name='Petr', last_name='Ivanov')
        self.book1 = Book.objects.create(name="Book name", price=232.00, author_name='Author 1', owner=user1)

        UserBookRelation.objects.create(user=user1, book=self.book1, like=True, rate=5)
        UserBookRelation.objects.create(user=user2, book=self.book1, like=True, rate=5)
        UserBookRelation.objects.create(user=user3, book=self.book1, like=True, rate=4)


    def test_ok(self):
        set_rating(self.book1)
        self.book1.refresh_from_db()
        self.assertEqual('4.67', str(self.book1.rating))