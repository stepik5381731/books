import json

from django.contrib.auth.models import User
from django.db import connection
from django.db.models import Count, Case, When, Avg
from django.urls import reverse
from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITestCase

from apps.store.models import Book, UserBookRelation
from apps.store.serializers import BookSerializer
from django.test.utils import CaptureQueriesContext


class BooksAPITestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='test username')
        self.user2 = User.objects.create(username='test username 2')
        self.user_staff = User.objects.create(username='staff username', is_staff=True)
        self.book1 = Book.objects.create(name="Test Book name", price=232, author_name="Test author 1", owner=self.user)
        self.book2 = Book.objects.create(name="Test Book name 2", price=850, author_name="Test author 2")
        self.book3 = Book.objects.create(name="Test Book name 3 Test author 1", price=33, author_name="Test author 3")

        UserBookRelation.objects.create(user=self.user, book=self.book1, like=True, rate=5)

    def test_get(self):
        url = reverse('book-list')
        with CaptureQueriesContext(connection) as queries:
            response = self.client.get(url)
            self.assertEqual(2, len(queries))
        books = Book.objects.all().annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))), ).order_by('id')
        serializer_data = BookSerializer(books, many=True).data
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)
        self.assertEqual(serializer_data[0]['rating'], '5.00')
        self.assertEqual(serializer_data[0]['annotated_likes'], 1)

    def test_get_filter(self):
        url = reverse('book-list')
        response = self.client.get(url, data={'price': 33})
        books = Book.objects.filter(id__in=[self.book3.id]).annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))), )
        serializer_data = BookSerializer(books, many=True).data
        self.assertEqual(serializer_data, response.data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_get_search(self):
        url = reverse('book-list')
        response = self.client.get(url, data={'search': 'Test author 1'})
        books = Book.objects.filter(id__in=[self.book1.id, self.book3.id]).annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))), ).order_by('id')
        serializer_data = BookSerializer(books, many=True).data
        self.assertEqual(serializer_data, response.data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_create(self):
        self.assertEqual(3, Book.objects.all().count())
        url = reverse('book-list')

        data = {
            'name': 'Programming on Python',
            'price': 165,
            'author_name': 'Mark Summerfield'
        }
        self.client.force_login(self.user)
        json_data = json.dumps(data)
        response = self.client.post(url, data=json_data, content_type='application/json')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(4, Book.objects.all().count())
        self.assertEqual(Book.objects.last().owner, self.user)

    def test_update(self):
        url = reverse('book-detail', args=(self.book1.id,))

        data = {
            'name': self.book1.name,
            'price': 500,
            'author_name': self.book1.author_name
        }
        self.client.force_login(self.user)
        json_data = json.dumps(data)
        response = self.client.put(url, data=json_data, content_type='application/json')
        # self.book1 = Book.objects.get(id=self.book1.id)
        self.book1.refresh_from_db()
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(500, self.book1.price)

    def test_delete(self):
        self.assertEqual(3, Book.objects.all().count())
        url = reverse('book-detail', args=(self.book1.id,))

        self.client.force_login(self.user)

        response = self.client.delete(url)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(2, Book.objects.all().count())

    def test_update_not_owner(self):
        url = reverse('book-detail', args=(self.book1.id,))

        data = {
            'name': self.book1.name,
            'price': 500,
            'author_name': self.book1.author_name
        }
        self.client.force_login(self.user2)
        json_data = json.dumps(data)
        response = self.client.put(url, data=json_data, content_type='application/json')
        # self.book1 = Book.objects.get(id=self.book1.id)
        self.book1.refresh_from_db()
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertEqual({'detail': ErrorDetail(string='You do not have permission to perform this action.',
                                                code='permission_denied')}, response.data)
        self.assertEqual(232, self.book1.price)

    def test_delete_not_owner(self):
        self.assertEqual(3, Book.objects.all().count())
        url = reverse('book-detail', args=(self.book1.id,))

        self.client.force_login(self.user2)

        response = self.client.delete(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertEqual({'detail': ErrorDetail(string='You do not have permission to perform this action.',
                                                code='permission_denied')}, response.data)
        self.assertEqual(3, Book.objects.all().count())

    def test_update_not_owner_but_staff(self):
        url = reverse('book-detail', args=(self.book1.id,))

        data = {
            'name': self.book1.name,
            'price': 500,
            'author_name': self.book1.author_name
        }
        self.client.force_login(self.user_staff)
        json_data = json.dumps(data)
        response = self.client.put(url, data=json_data, content_type='application/json')
        # self.book1 = Book.objects.get(id=self.book1.id)
        self.book1.refresh_from_db()
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(500, self.book1.price)

    def test_delete_not_owner_but_staff(self):
        self.assertEqual(3, Book.objects.all().count())
        url = reverse('book-detail', args=(self.book1.id,))

        self.client.force_login(self.user_staff)

        response = self.client.delete(url)
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(2, Book.objects.all().count())


class BooksRelationTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='test username')
        self.user2 = User.objects.create(username='test username 2')
        self.user_staff = User.objects.create(username='staff username', is_staff=True)
        self.book1 = Book.objects.create(name="Test Book name", price=232, author_name="Test author 1", owner=self.user)
        self.book2 = Book.objects.create(name="Test Book name 2", price=850, author_name="Test author 2")
        self.book3 = Book.objects.create(name="Test Book name 3 Test author 1", price=33, author_name="Test author 3")

    def test_like(self):
        url = reverse('userbookrelation-detail', args=(self.book1.id,))
        data = {
            'like': True,
        }

        json_data = json.dumps(data)
        self.client.force_login(self.user)
        response = self.client.patch(url, data=json_data, content_type='application/json')
        self.book1.refresh_from_db()
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        relation = UserBookRelation.objects.get(user=self.user, book=self.book1)
        self.assertTrue(relation.like)

        data = {
            'in_bookmarks': True,
        }
        json_data = json.dumps(data)
        response = self.client.patch(url, data=json_data, content_type='application/json')
        self.book1.refresh_from_db()
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        relation = UserBookRelation.objects.get(user=self.user, book=self.book1)
        self.assertTrue(relation.in_bookmarks)

    def test_rate(self):
        url = reverse('userbookrelation-detail', args=(self.book1.id,))
        data = {
            'rate': 3,
        }

        json_data = json.dumps(data)
        self.client.force_login(self.user)
        response = self.client.patch(url, data=json_data, content_type='application/json')
        self.book1.refresh_from_db()
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        relation = UserBookRelation.objects.get(user=self.user, book=self.book1)
        self.assertEqual(3, relation.rate)
