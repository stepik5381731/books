from django.contrib.auth.models import User
from django.db.models import Count, Case, When, Avg
from django_filters.compat import TestCase

from apps.store.models import Book, UserBookRelation
from apps.store.serializers import BookSerializer


class BookSerializerTestCase(TestCase):

    def test_ok(self):
        user1 = User.objects.create(username='test username 1', first_name='Ivan', last_name='Petrov')
        user2 = User.objects.create(username='test username 2', first_name='Ivan', last_name='Sidor')
        user3 = User.objects.create(username='test username 3', first_name='Petr', last_name='Ivanov')
        book1 = Book.objects.create(name="Book name", price=232.00, author_name='Author 1', owner=user1)
        book2 = Book.objects.create(name="Book name 2", price=850.00, author_name='Author 2')

        UserBookRelation.objects.create(user=user1, book=book1, like=True, rate=5)
        UserBookRelation.objects.create(user=user2, book=book1, like=True, rate=5)
        UserBookRelation.objects.create(user=user3, book=book1, like=True, rate=4)

        UserBookRelation.objects.create(user=user1, book=book2, like=True, rate=3)
        UserBookRelation.objects.create(user=user2, book=book2, like=True, rate=4)
        UserBookRelation.objects.create(user=user3, book=book2, like=False)

        books = Book.objects.all().annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
        ).order_by('id')
        data = BookSerializer(books, many=True).data
        expected_data = [
            {
                'id': book1.id,
                'name': "Book name",
                'price': '232.00',
                'author_name': 'Author 1',
                'annotated_likes': 3,
                'rating': '4.67',
                'owner_name': 'test username 1',
                'readers': [
                    {
                        'first_name': 'Ivan',
                        'last_name': 'Petrov'
                    },
                    {
                        'first_name': 'Ivan',
                        'last_name': 'Sidor'
                    },
                    {
                        'first_name': 'Petr',
                        'last_name': 'Ivanov'
                    },
                ]

            },
            {
                'id': book2.id,
                'name': "Book name 2",
                'price': '850.00',
                'author_name': 'Author 2',
                'annotated_likes': 2,
                'rating': '3.50',
                'owner_name': '',
                'readers': [
                    {
                        'first_name': 'Ivan',
                        'last_name': 'Petrov'
                    },
                    {
                        'first_name': 'Ivan',
                        'last_name': 'Sidor'
                    },
                    {
                        'first_name': 'Petr',
                        'last_name': 'Ivanov'
                    },
                ]
            }
        ]
        print(data[0]['readers'])
        print(expected_data[0]['readers'])
        self.assertEqual(expected_data, data)
