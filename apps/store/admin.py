from django.contrib import admin

from apps.store.models import Book, UserBookRelation

admin.site.register(Book)
admin.site.register(UserBookRelation)
